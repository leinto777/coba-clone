package com.example.simplenetworking.api

import com.example.simplenetworking.model.CategoryMenu
import com.example.simplenetworking.model.OrderRequest
import com.example.simplenetworking.model.OrderResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/* TODO LIST 2: Buat Interface API. yang memanggil endpointnya
* */
interface APIService {
    //untuk mapping Endpoint API
    @GET("category-menu")
    fun getCategory(): Call<CategoryMenu>

    @GET("category-menu")
    suspend fun getCategory2(): CategoryMenu
    @POST("order")
    fun postOrder(@Body orderRequest: OrderRequest): Call<OrderResponse>
}