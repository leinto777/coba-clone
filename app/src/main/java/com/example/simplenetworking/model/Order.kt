package com.example.simplenetworking.model

data class Order(
    val catatan: String?,
    val harga: Int?,
    val nama: String?,
    val qty: Int?
)