package com.example.simplenetworking.model

data class CategoryMenu(
    val code: Int?,
    val `data`: List<CategoryData?>?,
    val message: String?,
    val status: Boolean?
)