package com.example.simplenetworking.model

data class OrderResponse(
    val code: Int?,
    val message: String?,
    val status: Boolean?
)